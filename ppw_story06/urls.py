from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('form',views.index, name='index'),
    path('profile', views.profile, name='profile'),
    path('add_status', views.add_status, name='add_status'),
    path('view', views.message_table, name='view'),
]
