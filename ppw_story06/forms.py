from django import forms

class Form06(forms.Form):
    error_messages = {
        'required': 'Please fill in the input',
    }
    status_attrs = {
        'type': 'text',
        'class': 'form06-input',
        'placeholder':'Input status'
    }

    status = forms.CharField(label='', required=True, max_length=200, widget=forms.Textarea(attrs={'rows': 5, 'cols': 150}))
