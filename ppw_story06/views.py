from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Form06
from .models import Status

# Create your views here.
response = {}
def index(request):
    response['author'] = "Jeffrey"
    status = Status.objects.all()
    response['status'] = status
    html = 'ppw_story06/index.html'
    response['form06'] = Form06
    return render(request, html, response)

def profile(request):
    return render(request,'ppw_story06/profile.html',{})

def add_status(request):
    form = Form06(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        status = Status(status=response['status'])
        status.save()
        return HttpResponseRedirect('/ppw_story06/view')
    else:
        return HttpResponseRedirect('/ppw_story06/view')

def message_table(request):
    message = Status.objects.all()
    response['output'] = message
    html = 'ppw_story06/index.html'
    return render(request, html , response)
