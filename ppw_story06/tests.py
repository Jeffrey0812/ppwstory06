from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status, profile
from .models import Status
from .forms import Form06
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time




class Story06Test(TestCase):
    def test_story06_url_is_exist(self):
        response = Client().get('/ppw_story06/')
        self.assertEqual(response.status_code,200)

    def test_story06_using_index_func(self):
        found = resolve('/ppw_story06/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_status(self):
        # Creating a new status
        new_status = Status.objects.create(status='mengerjakan lab ppw')

        # Retrieving all available activity
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
        form = Form06(data={'Status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."]
        )

    def test_story06_post_success_and_render_the_result(self):
        test = 'anonymous'
        response_post = Client().post('/ppw_story06/add_status', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/ppw_story06/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story06_post_error_and_render_the_result(self):
        test = 'yolo'
        response_post = Client().post('/ppw_story06/add_status', {'status': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/ppw_story06/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    #Challenge
    def test_profile_url_is_exist(self):
        response = Client().get('/ppw_story06/profile')
        self.assertEqual(response.status_code,200)

    def test_story06_using_index_func(self):
        found = resolve('/ppw_story06/profile')
        self.assertEqual(found.func, profile)

    def test_title_profile(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title> Story 6 - Profile</title>', html_response)
'''
class Story07FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story07FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story07FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://ppw-story06.herokuapp.com/ppw_story06')
        time.sleep(3)
        # find the form element
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        status.send_keys('cobacoba')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        time.sleep(3)

        self.assertIn("cobacoba", selenium.page_source)

    def test_positioning_1(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://ppw-story06.herokuapp.com/ppw_story06')
        self.assertIn('<h1 align="center"> Hello apa kabar? </h1>', selenium.page_source)

    def test_positioning_2(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://ppw-story06.herokuapp.com/ppw_story06')
        self.assertIn('submit', selenium.page_source)

    def test_style_1(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://ppw-story06.herokuapp.com/ppw_story06')
        self.assertIn('<input id="submit" type="submit" class="btn btn-lg btn-block btn-info" value="Submit">', selenium.page_source)

    def test_style_1(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://ppw-story06.herokuapp.com/ppw_story06')
        self.assertIn('<textarea name="status" cols="150" rows="5" maxlength="200" required="" id="id_status"></textarea>', selenium.page_source)
'''
